import cv2 as cv
import numpy as np
import time

# Task
# Write a Python program to capture frames from your webcam for around one minute (depending on what you actually do).
# Divide time into 10-second periods: during each period, do one of these tasks on the frames:
# 1) make them grayscale,
# 2) write some (floating) text on them and draw emojis,
# 3) blend them with some other image (which could be your logo),
# 4) copy some part of the frames and paste at other parts (could be floating, or could be a complete shuffle of the whole frame), and
# 5) replacing some colors of your choice. Bonus: if you use additional features of OpenCV.


def grayscale(frame):
    gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    return cv.cvtColor(gray, cv.COLOR_GRAY2BGR)


def emoji(frame, start_x, start_y, end_x, end_y):
    w = end_x - start_x
    h = end_y - start_y
    r = max(w//2, h//2)
    c = (start_x + r, start_y + r)

    frame = cv.circle(frame, c, r, (0, 255, 255), -1)

    frame = cv.circle(frame, (c[0] + r//2, c[1] - r//4),
                      r//4, (0, 0, 0), -1)
    frame = cv.circle(frame, (c[0] - r//2, c[1] - r//4),
                      r//4, (0, 0, 0), -1)

    frame = cv.ellipse(frame, (c[0], c[1] + r//4), (r//3, r//7), 0,
                       0, 180, (0, 0, 0), 2)


def text(frame, elapsed_time):
    t_pos = (int(200 + np.sin(elapsed_time) * 50),
             int(200 + np.cos(elapsed_time) * 50))
    frame = cv.putText(frame, 'OpenCV', t_pos, cv.FONT_HERSHEY_SIMPLEX,
                       2, (255, 255, 255), 2, cv.LINE_AA)


def shift(frame):
    for y in range(0, frame.shape[0], 2):
        frame[y, :] = frame[y, ::-1]


def blend(frame, logo):
    alpha = 0.8
    return cv.addWeighted(frame, alpha, logo, 1-alpha, 0.0)


def replace_color(frame, color, new_color, sensitivity=10):
    hsv = cv.cvtColor(frame, cv.COLOR_BGR2HSV)
    hue = cv.cvtColor(color, cv.COLOR_BGR2HSV)[0][0][0]
    lower = np.array([hue-sensitivity, 100, 100])
    upper = np.array([hue+sensitivity, 255, 255])
    mask = cv.inRange(hsv, lower, upper)
    frame[mask != 0] = new_color


def detect_face(frame, model):
    # Get width and height of the image
    h, w = frame.shape[:2]
    # Preprocess the image: resize and performs mean subtraction
    blob = cv.dnn.blobFromImage(frame, 1.0, (300, 300), (104.0, 177.0, 123.0))
    # Set the image into the input of the neural network
    model.setInput(blob)
    # Perform inference and get the result
    output = np.squeeze(model.forward())
    for i in range(0, output.shape[0]):
        # Get the confidence
        confidence = output[i, 2]
        # If confidence is above 50%, then draw the surrounding box
        if confidence > 0.5:
            # Get the surrounding box cordinates and upscale them to original image
            box = output[i, 3:7] * np.array([w, h, w, h])
            # Convert to integers
            start_x, start_y, end_x, end_y = box.astype(int)
            # Draw
            emoji(frame, start_x, start_y, end_x, end_y)


def main():
    logo = cv.imread("OpenCV_logo_black_.png")

    prototxt_path = "weights/deploy.prototxt.txt"
    model_path = "weights/res10_300x300_ssd_iter_140000_fp16.caffemodel"
    model = cv.dnn.readNetFromCaffe(prototxt_path, model_path)

    cap = cv.VideoCapture(0)
    if not cap.isOpened():
        print("Failed to open camera")
        exit(1)
    else:
        print("Opened camera")

    fps = cap.get(cv.CAP_PROP_FPS)

    fourcc = cv.VideoWriter_fourcc(*"XVID")
    out = cv.VideoWriter("output.avi", fourcc, fps, (640, 480))

    start_time = time.time()
    while True:
        ret, frame = cap.read()
        if not ret:
            print("Failed to read frame")
            break
        elapsed_time = time.time() - start_time

        if logo.shape != frame.shape:
            logo = cv.resize(logo, frame.shape[1::-1])

        if elapsed_time > 50:
            exit()
        if elapsed_time > 40:
            replace_color(frame, np.uint8(
                [[[255, 0, 0]]]), np.uint8([0, 255, 0]))
        elif elapsed_time > 30:
            shift(frame)
        elif elapsed_time > 20:
            frame = blend(frame, logo)
        elif elapsed_time > 10:
            detect_face(frame, model)
            text(frame, elapsed_time)
        else:
            frame = grayscale(frame)

        out.write(frame)
        cv.imshow("Camera", frame)
        if cv.waitKey(1) == ord('q'):
            break

    cap.release()
    out.release()
    cv.destroyAllWindows()


if __name__ == "__main__":
    main()
